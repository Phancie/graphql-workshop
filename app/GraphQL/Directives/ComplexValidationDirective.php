<?php

namespace App\GraphQL\Directives;

use Nuwave\Lighthouse\Schema\Directives\BaseDirective;
use Nuwave\Lighthouse\Schema\Directives\ValidationDirective;


class ComplexValidationDirective extends ValidationDirective
{
    // TODO implement the directive https://lighthouse-php.com/master/custom-directives/getting-started.html

    /**
     * Return validation rules for the arguments.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        // TODO: Implement rules() method.
        return[
            "first_name" => ["min:3"],
            "last_name" => ["max:5"],
            "phone_number" => ["min:10","max:15"]
        ];
    }
}
