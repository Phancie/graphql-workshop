<?php

namespace App\GraphQL\Queries;

use App\Customer;

class SomeComplexQuery
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        // TODO implement the resolver
        return Customer::find($args['id']);
    }
}
